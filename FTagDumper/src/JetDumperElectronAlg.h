#ifndef SINGLE_BTAG_ALG_E_HH
#define SINGLE_BTAG_ALG_E_HH

#include "xAODJet/JetContainer.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"

#include <memory>

class BTagElectronAugmenter;
class AsgElectronSelectorTool;

class JetDumperElectronAlg: public AthAlgorithm
{
public:
  JetDumperElectronAlg(const std::string& name, ISvcLocator* pSvcLocator);
  ~JetDumperElectronAlg();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
private:
  std::unique_ptr<BTagElectronAugmenter> m_electron_augmenter;
  std::unique_ptr<AsgElectronSelectorTool> m_electron_select;
};

#endif // SINGLE_BTAG_ALG_E_HH
