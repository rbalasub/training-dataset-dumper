#!/usr/bin/env python
"""
Dump some ftag info with a custom DR=0.5 fixed cone track association; after track association, 
tracks are selected according with the score of the nntc (neural-net-track-classifier),
defined in athena: FlavourTagDiscriminants/ITrackClassifier.h.

The fixed cone association is also used for the training of the nntc model.

The nntc model is specified in the EMPFlow_fixedcone.json config file, as "nntc" with the path to the json file of the network.
The track association algorithm can be accessed in the pflow_fixedcone_dumper function defined below.
The size of the cone is parametrized by the fixedConeRadius parameter.

Note that your configuration file must specify the correct name for
the tracks associated to the b-tagging object: the links in
"BTagTrackToJetAssociator" will still point to the default
association.

Grid info:
It is recommended to submit jobs (grid-submit) with the -m flag; see https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/merge_requests/392#7deb79e71f2d478961da0bf7ce92f1292596c303 for more infos.
"""

from FTagDumper import dumper

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

import sys

def pflow_fixedcone_dumper(cfgFlags,an):
    
    from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleFixedConeAssociationAlgCfg
   
    ca = ComponentAccumulator()
    temp_jets='AntiKt4EMPFlowJets'
    fs_tracks='InDetTrackParticles'
    bc='BTagging_AntiKt4EMPFlow'
    trackOnJetDecorator = f'{temp_jets}.{an}ForBTagging'

    ca.merge(JetParticleFixedConeAssociationAlgCfg(
        cfgFlags,
        fixedConeRadius=0.5,
        JetCollection=temp_jets,
        InputParticleCollection=fs_tracks,
        OutputParticleDecoration=trackOnJetDecorator.split('.')[-1]
    ))
    Copier = CompFactory.FlavorTagDiscriminants.BTagTrackLinkCopyAlg
    copier = Copier(
        'TrackCopier',
        jetTracks=trackOnJetDecorator,
        btagTracks=f'{bc}.{an}',
        jetLinkName=f'{bc}.jetLink'
    )
    ca.addEventAlgo(copier)
    return ca

def run():

    args = dumper.base_parser(__doc__).parse_args()
    
    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags    
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = dumper.getMainConfig(cfgFlags, args)

    _default_assoc = 'FixedCone_Tracks'
    ca.merge(pflow_fixedcone_dumper(cfgFlags,_default_assoc))
    ca.merge(dumper.getDumperConfig(args))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
